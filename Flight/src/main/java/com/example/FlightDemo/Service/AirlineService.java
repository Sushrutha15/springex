package com.example.FlightDemo.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.FlightDemo.Modal.Airline;
import com.example.FlightDemo.Modal.Flight;
import com.example.FlightDemo.Modal.Inventory;
import com.example.FlightDemo.Modal.TicketBooking;
import com.example.FlightDemo.Repository.AirlineRepository;
import com.example.FlightDemo.Repository.FlightRepository;
import com.example.FlightDemo.Repository.InventoryRepository;
import com.example.FlightDemo.Repository.TicketBookRepository;

@Service
public class AirlineService {
	
	@Autowired
   InventoryRepository inventoryRepository;

	@Autowired
	FlightRepository flightRepository;
	
	@Autowired
	TicketBookRepository ticketBookRepository;
	
	@Autowired
	AirlineRepository airlineRepository;
	public void saveInventory(Inventory inventory) {
		inventoryRepository.save(inventory);
	}
	public void saveTicket(TicketBooking ticketBooking) {
		ticketBookRepository.save(ticketBooking);
	}
	
	public List<Flight> getFlightList(Flight flightsent) {
		
		List<Flight> flightList= new ArrayList<Flight>();
		List<Flight> flightRepo =flightRepository.findAll();
		for(Flight f:flightRepo)
		{
			if(f.getDate().contentEquals(flightsent.getDate()) && 
			(f.getTime().contentEquals(flightsent.getTime()) &&
			(f.getFromPlace().contentEquals(flightsent.getFromPlace()) && 
			(f.getToPlace().contentEquals(flightsent.getToPlace()))  )))
			{
				flightList.add(f);
			}
		}
		return flightList;
	}
	public void saveFlight(Flight flight) {
		flightRepository.save(flight);
	}
	public TicketBooking getTicketByPnr(int pnr) {
		List<TicketBooking> ticketList = ticketBookRepository.findAll();
		for(TicketBooking t:ticketList)
		{
			if(t.getPnr()==pnr)
				return t;
		}
		return null;
	}
	
	public TicketBooking getTicketByEmail(String email) {
		List<TicketBooking> emailTicketList= ticketBookRepository.findAll();
		for(TicketBooking t1:emailTicketList)
		{
			if(t1.getEmail().contentEquals(email))
			{
				return t1;
			}
		}
		return null;
	}
	public void register(Airline airline) {
		airlineRepository.save(airline);
	}
	
	
}
