package com.example.FlightDemo.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.FlightDemo.Modal.Airline;
import com.example.FlightDemo.Modal.Flight;
import com.example.FlightDemo.Modal.Inventory;
import com.example.FlightDemo.Modal.TicketBooking;
import com.example.FlightDemo.Repository.TicketBookRepository;
import com.example.FlightDemo.Service.AirlineService;

@RestController
@RequestMapping
public class FlightController {
	
	@Autowired
	AirlineService airlineService;
	
	@Autowired
	TicketBookRepository ticketBookRepository;

	@PostMapping("/airline/register")
	public String airLineBooking(@RequestBody Airline airline)
	{
		airlineService.register(airline);
		return "Airline Name : "+airline.getAirlineName()+"\n"+
	"Contact Phone : "+airline.getContactNumber()+"\n"+"Contact Address : "+airline.getContactAddress();
	}
	
	@PostMapping("/airline/inventory/add")
	public String addInventory(@RequestBody Inventory inventory)
	{
		airlineService.saveInventory(inventory);
		return ""; 
	}
	
	@PostMapping("/airline/flight")
	public void getFlight(@RequestBody Flight flight)
	{
		airlineService.saveFlight(flight);
	}
	
	@PostMapping("/flight/search")
	public List<Flight> Search(@RequestBody Flight flight)
	{
		 return airlineService.getFlightList(flight);
	}
	
	@PostMapping("/flight/booking/")
	public String bookTicket(@RequestBody TicketBooking ticketBooking)
	{
		ticketBooking.setPnr((int) Math.floor(Math.random()*10)+1);
		airlineService.saveTicket(ticketBooking);
		return null;
	}
	
	@GetMapping("/flight/ticket/{pnr}")
	public TicketBooking ticketDetails(@PathVariable int pnr)
	{
		return airlineService.getTicketByPnr(pnr);	
	}
	
	@GetMapping("/flight/booking/history/{email}")
	public TicketBooking ticketByEmail(@PathVariable String email)
	{
		return airlineService.getTicketByEmail(email);
	}
	
	@DeleteMapping("/flight/booking/cancel/{pnr}")
	public void deleteTicket(@PathVariable int pnr)
	{
		List<TicketBooking> deleteTicketByPnr = ticketBookRepository.findAll();
		for(TicketBooking d1 : deleteTicketByPnr)
		{
			if(d1.getPnr()==pnr)
				ticketBookRepository.delete(d1);
		}
	}
}
