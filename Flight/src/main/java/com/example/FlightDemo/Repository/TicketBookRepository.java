package com.example.FlightDemo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.FlightDemo.Modal.TicketBooking;

@Repository
public interface TicketBookRepository extends JpaRepository<TicketBooking, Integer>{

}
