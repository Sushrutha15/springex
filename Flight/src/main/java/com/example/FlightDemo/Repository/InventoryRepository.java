package com.example.FlightDemo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.FlightDemo.Modal.Inventory;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory, Integer>{

}
