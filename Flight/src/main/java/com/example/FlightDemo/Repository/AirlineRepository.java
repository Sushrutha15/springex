package com.example.FlightDemo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.FlightDemo.Modal.Airline;

@Repository
public interface AirlineRepository extends JpaRepository<Airline, Integer>{

}
