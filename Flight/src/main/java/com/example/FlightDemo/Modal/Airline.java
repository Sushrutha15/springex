package com.example.FlightDemo.Modal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Airline {
	
@Id
 private int airlineId;
 private String airlineName;
 private String contactNumber;
 private String contactAddress;
 
public int getAirlineId() {
	return airlineId;
}
public void setAirlineId(int airlineId) {
	this.airlineId = airlineId;
}
public String getAirlineName() {
	return airlineName;
}
public void setAirlineName(String airlineName) {
	this.airlineName = airlineName;
}
public String getContactNumber() {
	return contactNumber;
}
public void setContactNumber(String contactNumber) {
	this.contactNumber = contactNumber;
}
public String getContactAddress() {
	return contactAddress;
}
public void setContactAddress(String contactAddress) {
	this.contactAddress = contactAddress;
}
 
}
