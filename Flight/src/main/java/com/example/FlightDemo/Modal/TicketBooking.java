package com.example.FlightDemo.Modal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class TicketBooking {

@Id
public int fid;
private int tid;
private String name;
private String email;
private int seats;
private String passName;
private String passGender;
private int passAge;
private String meals;
private int selectSeats;
private int pnr;
public int getTid() {
	return tid;
}
public void setTid(int tid) {
	this.tid = tid;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public int getSeats() {
	return seats;
}
public void setSeats(int seats) {
	this.seats = seats;
}
public String getPassName() {
	return passName;
}
public void setPassName(String passName) {
	this.passName = passName;
}
public String getPassGender() {
	return passGender;
}
public void setPassGender(String passGender) {
	this.passGender = passGender;
}
public int getPassAge() {
	return passAge;
}
public void setPassAge(int passAge) {
	this.passAge = passAge;
}
public int getSelectSeats() {
	return selectSeats;
}
public void setSelectSeats(int selectSeats) {
	this.selectSeats = selectSeats;
}
public String getMeals() {
	return meals;
}
public void setMeals(String meals) {
	this.meals = meals;
}
public int getPnr() {
	return pnr;
}
public void setPnr(int pnr) {
	this.pnr = pnr;
}
public int getFid() {
	return fid;
}
public void setFid(int fid) {
	this.fid = fid;
}

}
