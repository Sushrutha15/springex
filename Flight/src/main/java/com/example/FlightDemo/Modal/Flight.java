package com.example.FlightDemo.Modal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Flight {
	
@Id
public int fid;
private String date;
private String time;
private String fromPlace;
private String toPlace;
private String tripType;
private String airLineName;
private String airLineLogo;
private int contactNumber;
private String contactAddress; 
private String price;
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}
public String getTime() {
	return time;
}
public void setTime(String time) {
	this.time = time;
}
public String getFromPlace() {
	return fromPlace;
}
public void setFromPlace(String fromPlace) {
	this.fromPlace = fromPlace;
}
public String getToPlace() {
	return toPlace;
}
public void setToPlace(String toPlace) {
	this.toPlace = toPlace;
}
public String getTripType() {
	return tripType;
}
public void setTripType(String tripType) {
	this.tripType = tripType;
}
public String getAirLineName() {
	return airLineName;
}
public void setAirLineName(String airLineName) {
	this.airLineName = airLineName;
}
public String getAirLineLogo() {
	return airLineLogo;
}
public void setAirLineLogo(String airLineLogo) {
	this.airLineLogo = airLineLogo;
}
public String getPrice() {
	return price;
}
public void setPrice(String price) {
	this.price = price;
}
public int getContactNumber() {
	return contactNumber;
}
public void setContactNumber(int contactNumber) {
	this.contactNumber = contactNumber;
}
public String getContactAddress() {
	return contactAddress;
}
public void setContactAddress(String contactAddress) {
	this.contactAddress = contactAddress;
}
public int getFid() {
	return fid;
}
public void setFid(int fid) {
	this.fid = fid;
}


}
