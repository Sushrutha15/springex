package com.example.FlightDemo.Modal;

//import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="inventory")
public class Inventory {

@Id
private int id;
private String flightNo;
private String airLine;
private String fromPlace;
private String toPlace;
private String startDateTime;
private String endDateTime;
private String scheduledDays;
private String instruments;
private int businessSeats;
private int nonbusinessSeats;
private double ticketCost;
private int noofrows;
private String meals;
public String getFlightNo() {
	return flightNo;
}
public void setFlightNo(String flightNo) {
	this.flightNo = flightNo;
}
public String getAirLine() {
	return airLine;
}
public void setAirLine(String airLine) {
	this.airLine = airLine;
}
public String getFromPlace() {
	return fromPlace;
}
public void setFromPlace(String fromPlace) {
	this.fromPlace = fromPlace;
}
public String getToPlace() {
	return toPlace;
}
public void setToPlace(String toPlace) {
	this.toPlace = toPlace;
}
public String getStartDateTime() {
	return startDateTime;
}
public void setStartDateTime(String startDateTime) {
	this.startDateTime = startDateTime;
}
public String getEndDateTime() {
	return endDateTime;
}
public void setEndDateTime(String endDateTime) {
	this.endDateTime = endDateTime;
}
public int getBusinessSeats() {
	return businessSeats;
}
public void setBusinessSeats(int businessSeats) {
	this.businessSeats = businessSeats;
}
public int getNonbusinessSeats() {
	return nonbusinessSeats;
}
public void setNonbusinessSeats(int nonbusinessSeats) {
	this.nonbusinessSeats = nonbusinessSeats;
}
public double getTicketCost() {
	return ticketCost;
}
public void setTicketCost(double ticketCost) {
	this.ticketCost = ticketCost;
}

public int getNoofrows() {
	return noofrows;
}
public void setNoofrows(int noofrows) {
	this.noofrows = noofrows;
}
public String getScheduledDays() {
	return scheduledDays;
}
public void setScheduledDays(String scheduledDays) {
	this.scheduledDays = scheduledDays;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getInstruments() {
	return instruments;
}
public void setInstruments(String instruments) {
	this.instruments = instruments;
}
public String getMeals() {
	return meals;
}
public void setMeals(String meals) {
	this.meals = meals;
}


}
