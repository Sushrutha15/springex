package com.example.springDemo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {
	
	@Autowired
	AccountService accountService;

@PostMapping("/account")
public String getAccount(@RequestBody Account account)
{
	accountService.save(account);
	System.out.println("Owner Name : "+account.getOwnerName());
	System.out.println("Address : "+account.getCity()+" "+account.getState()+" "+account.getPin());
	System.out.println("Balance Amount : "+account.getBalAmt());
	System.out.println("Created Date : "+account.getDate());
	System.out.println("Status : "+account.getStatus());
	return "account";
}

@GetMapping("/get/{AccountId}")
public String AccountDetails(@PathVariable int AccountId)
{
	System.out.println("Account Id : "+ AccountId);
	return "Account ID" ;
}

@GetMapping("/getAccount/{name}")
public String getName(@PathVariable String name)
{
	System.out.println("Owner Name : "+ name);
	return "Name : "+name;
}

ArrayList<Account> accountList= new ArrayList<Account>();
@PostMapping("/array")
public String array(@RequestBody Account getAccount)
{
	
	System.out.println(getAccount.getOwnerName());
	System.out.println(getAccount.getCity());
	System.out.println(getAccount.getState());
	System.out.println(getAccount.getPin());
	System.out.println(getAccount.getBalAmt());
	System.out.println(getAccount.getDate());
	System.out.println(getAccount.getStatus());
	accountList.add(getAccount);
			return "getAccount";
}

@GetMapping("/retrieve")
public ArrayList<Account> retrieve()
{
	return accountList;
}

@GetMapping("/list")
public List<Account> getAccountDetails()
{
	System.out.println("List of Account Details is printed");
	return accountService.getAccountDetails();
}
}
