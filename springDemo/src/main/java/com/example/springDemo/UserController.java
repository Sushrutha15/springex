package com.example.springDemo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	
	@Autowired
	UserService userService;

	@GetMapping("/{id}")
	void getUsers(@PathVariable Integer id)
	{
		System.out.println("called" +id);
	}
	
	@PostMapping("/{id}")
	public String getUser(@RequestBody User user)
	{
		System.out.println("got User "+user.getName());
		return "Post Called";
	}
	
	@PutMapping
	public String putcall()
	{
		System.out.println("Put");
		return "Put call";
	}
		
	@PostMapping("/save")
	public String saveUser(@RequestBody User user)
	{
		userService.save(user);
		System.out.println("User Name : "+user.getName());
		System.out.println("Age : "+user.getAge());
		return user.getName()+"\n"+user.getAge();
	}
	
	@GetMapping("/users")
	public List<User> getUsers()
	{
		System.out.println("Users");
		return userService.getUsers();
		
	}
	}
