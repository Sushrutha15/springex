package com.example.springDemo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	public Integer save(User user)
	{
		userRepository.save(user);
		System.out.println(user);
		return user.getId();
	}
	public List<User> getUsers() {
		return userRepository.findAll();
	}
}
