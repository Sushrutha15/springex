package com.example.springDemo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

	@Autowired
	AccountRepository accountRepository;

	public void save(Account account) {
		accountRepository.save(account);
		
	}

	public List<Account> getAccountDetails() {
		return accountRepository.findAll();
	}
	
}
