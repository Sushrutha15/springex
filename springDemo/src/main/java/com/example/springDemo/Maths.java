package com.example.springDemo;

public class Maths {

	public int add(int i, int j) {
		
		return i+j;
	}

	public int subtract(int i, int j) {
		return i-j;
	}

	public int floorDiv(int i, int j) {
		return (i/j);
	}

	public float ceilDiv(float num, float den) {	
		return (num/den);
	}

}
