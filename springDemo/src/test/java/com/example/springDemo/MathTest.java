package com.example.springDemo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

class MathTest {

	@Test
	void testAdd() {
		Maths math =new Maths();
		int result=math.add(2,2);
		assertEquals(4, result);
	}

	@Test
	void testSubtract()
	{
		Maths math=new Maths();
		int result1=math.subtract(5,3);
		assertEquals(2,result1);
	}
	
	@Test
	void testDivide()
	{
		Maths math=new Maths();
		int result=math.floorDiv(5, 2);
		assertEquals(2,result);
	}
	
	@Test
	void testDivide1()
	{
		Maths math=new Maths();
		float result=math.ceilDiv(5, 2);
		assertEquals(2.5,result);
	}
}
