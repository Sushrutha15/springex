package com.xyzBank;

import java.util.Date;

public class Bank {
	public static void main(String args[])
	{
		Date date=new Date(2021,1,28);
		Address add=new Address("Hyd","Telangana",500086);
		System.out.println("---Savings Account Details--- \n");
		Savings s1=new Savings("Kaushik",add, 10000,date,"Active \n");
		
		System.out.println("Owner Name : "+ s1.getOwnerName());
		System.out.println("Address : "+s1.getAddress().getCity()+" "+s1.getAddress().getState()+" "+s1.getAddress().getPin());
		s1.deposit(10000);
		System.out.println(s1.withdraw(3000));
		System.out.println("Date : "+ s1.getDate());
		System.out.println("Status : "+ s1.getStatus());
		
		
		System.out.println("---Current Account Details--- \n");
		Current c1=new Current("Kaushik",add, 20000,date,"Active \n");
		System.out.println("Owner Name : "+ c1.getOwnerName());
		System.out.println("Address : "+c1.getAddress().getCity()+" "+c1.getAddress().getState()+" "+c1.getAddress().getPin());
		c1.deposit(10000);
		System.out.println(c1.withdraw(5000));
		System.out.println("Date : "+ c1.getDate());
		System.out.println("Status : "+ c1.getStatus());
	
	}
	
	

}
